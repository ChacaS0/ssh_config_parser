package ssh_config_parser

import (
	"errors"
)

// ErrHostNotFound is the error to return when no host has been found.
var ErrHostNotFound = errors.New("Host not found")

// GetConfigIndexByHost gives the index of the SshConfig holding the
// wanted `host`.
// See https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#SshConfig
func GetConfigIndexByHost(sshConfigs []SshConfig, host string) (int, error) {

	for k, v := range sshConfigs {
		if v.Host == host {
			return k, nil
		}
	}

	return -1, ErrHostNotFound
}
