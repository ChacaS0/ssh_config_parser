package ssh_config_parser

import (
	"fmt"
	"log"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func ExampleSegments_Parse() {
	raw := NewRawDataFromString(`# First block
Host sub.example.com
	IdentityFile ~/.ssh/sub.example.com
	Port 202
	User ssh_config_parser
	# ChacaS0 - gitlab.com
Host gitlab.com
	HostName gitlab.com
	IdentityFile ~/.ssh/gitlab
		`)

	segments, err := raw.GetSegments()
	if err != nil {
		log.Fatal("#1", err)
	}

	result, err := segments.Parse()
	if err != nil {
		log.Fatal("#1", err)
	}
	fmt.Println(result)
	// Output: [{sub.example.com                                   ~/.ssh/sub.example.com          202                 ssh_config_parser    } {gitlab.com                                 gitlab.com  ~/.ssh/gitlab                               }]
}

func TestSegments_Parse(t *testing.T) {
	raw1 := NewRawDataFromString(`# First block
Host sub.example.com
	IdentityFile ~/.ssh/sub.example.com
	Port 202
	User ssh_config_parser
	`)
	input1, _ := raw1.GetSegments()

	raw2 := NewRawDataFromString(`# ChacaS0 - gitlab.com
Host gitlab.com
	HostName gitlab.com
	IdentityFile ~/.ssh/gitlab
	`)
	input2, _ := raw2.GetSegments()

	testItems := []struct {
		Input         Segments
		ExpectedValue []SshConfig
		ExpectErr     error
	}{
		{
			input1,
			[]SshConfig{SshConfig{
				Host:         "sub.example.com",
				IdentityFile: "~/.ssh/sub.example.com",
				Port:         "202",
				User:         "ssh_config_parser",
			}},
			nil,
		},
		{
			input2,
			[]SshConfig{SshConfig{
				Host:         "gitlab.com",
				HostName:     "gitlab.com",
				IdentityFile: "~/.ssh/gitlab",
			}},
			nil,
		},
	}

	for _, v := range testItems {
		sshConfTest := SshConfig{}
		if got, err := v.Input.Parse(); !cmp.Equal(got, v.ExpectedValue) || v.ExpectErr != err {
			t.Error("[x] SshConfig.ParseFromSegments()\n\tgot:\n\t", sshConfTest, "\n\n\texpeceted value:\n\t", v.ExpectedValue, "\n\n\tgot error:\n\t", err, "\n\texpected error:\n\t", v.ExpectErr)
			t.Fail()
		}
	}
}
