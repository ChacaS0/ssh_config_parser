package ssh_config_parser

import "regexp"

// Segment is a block containing configurations
// that are related to each other,
// structured but not parsed yet.
type Segment struct {
	Content []byte
}

// IsEqual allows to compare two Segment(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segment).
// Returns true if they are equal.
func (s *Segment) IsEqual(segment Segment) bool {
	if len(s.Content) != len(segment.Content) {
		return false
	}

	for i, v := range s.Content {
		if v != segment.Content[i] {
			return false
		}
	}

	return true
}

// String returns the string version of the Segment(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segment).
func (s *Segment) String() string {
	return string(s.Content)
}

// NewSegmentFromString instanciate a new Segment(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segment)
// based on a string.
func NewSegmentFromString(str string) Segment {
	return Segment{[]byte(str)}
}

// getRows gives back a slice of string
// holding the Segment row by row.
// Each entry of the slice is a row from the Segment.
// See https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segment.
func (s *Segment) getRows() []string {
	var re = regexp.MustCompile(`(?sm)(\w+[\s\S]*?)$`)
	return re.FindAllString(s.String(), -1)
}
