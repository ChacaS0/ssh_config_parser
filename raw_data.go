package ssh_config_parser

import (
	"errors"
	"regexp"
	"strings"
)

// ErrNotingSegmented is raised when nothing could be segmented.
var ErrNotingSegmented = errors.New("Nothing segmented")

// RawData represents the data taken directly from
// the configuration file, e.g. :
// 	ioutil.Readfile(~/.ssh/config)
type RawData struct {
	Content []byte
}

// GetSegments cuts the raw data and organises it as Segments.
// Segments(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segments) is just a slice of Segment(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segment).
func (rd *RawData) GetSegments() (Segments, error) {
	segments := Segments{}
	if len(rd.Content) < 1 {
		return segments, ErrNotingSegmented
	}

	// Beta
	asString := rd.String()

	// Remove comments
	var re = regexp.MustCompile(`(?s)(\#.*?\n)`)
	asString = re.ReplaceAllString(asString, "")

	// Segment
	splitted := strings.Split(asString, `Host `)[1:]

	// Final format & append
	for _, v := range splitted {
		// splitted[i] = "Host " + v
		segments.Append(NewSegmentFromString(strings.TrimSpace("Host " + v)))
	}

	// If none found
	if len(segments.Content) < 1 {
		return segments, ErrNotingSegmented
	}

	return segments, nil
}

// NewRawData creates a new RawData from a []byte.
// See https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#RawData.
func NewRawData(rawData []byte) RawData {
	rawData = []byte(strings.Replace(string(rawData), "\r\n", "\n", -1))
	rawData = []byte(strings.Replace(string(rawData), "\r", "\n", -1))
	return RawData{Content: rawData}
}

// NewRawDataFromString creates a new RawData based on a string.
// It makes creating RawData a lot more convenient at times.
// See https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#RawData.
func NewRawDataFromString(str string) RawData {
	str = strings.Replace(str, "\r\n", "\n", -1)
	str = strings.Replace(str, "\r", "\n", -1)
	return RawData{[]byte(str)}
}

// String gives the string version of the RawData.
// See https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#RawData.
func (rd *RawData) String() string {
	return string(rd.Content)
}
