package ssh_config_parser

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

// Segments is a []Segment.
// @see https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segment.
type Segments struct {
	Content []Segment
}

// Parse takes in segments and spits out []SshConfig.
// See https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#SshConfig.
func (segs *Segments) Parse() (sshConfigs []SshConfig, err error) {
	var reSeg = regexp.MustCompile(`(?P<Param>\w+)[\s](?P<Value>.*)`)

	for _, seg := range segs.Content {
		sc := SshConfig{}
		rows := seg.getRows()
		for _, row := range rows {
			pair := reSeg.FindStringSubmatch(strings.TrimSpace(row))
			if len(pair) != 3 {
				return nil, errors.New("Cannot use this line from the configuration : " + row)
			}

			if err = sc.SetField(pair[1], pair[2]); err != nil {
				return nil, err
			}
		}
		sshConfigs = append(sshConfigs, sc)
	}

	return sshConfigs, nil
}

// Append adds a Segment(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segment) at the end of the Segments(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segments).
func (segs *Segments) Append(segment Segment) {
	segs.Content = append(segs.Content, segment)
}

// String gives a string version of the Segments(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segments).
func (segs *Segments) String() string {
	var endString string
	for _, v := range segs.Content {
		endString += fmt.Sprintln(v.String())
	}
	return endString
}

// IsEqual compares two Segments(https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser#Segments) and return true if
// they are considered as equal.
func (segs *Segments) IsEqual(segments Segments) bool {
	if len(segs.Content) != len(segments.Content) {
		return false
	}

	for i, v := range segs.Content {
		if !v.IsEqual(segments.Content[i]) {
			return false
		}
	}

	return true
}
