package ssh_config_parser

import (
	"fmt"
	"log"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func ExampleSshConfig_GetParams() {
	raw := NewRawDataFromString(`# First block
Host sub.example.com
	IdentityFile ~/.ssh/sub.example.com
	Port 202
	User ssh_config_parser
	# ChacaS0 - gitlab.com
Host gitlab.com
	HostName gitlab.com
	IdentityFile ~/.ssh/gitlab
		`)

	segments, err := raw.GetSegments()
	if err != nil {
		log.Fatal("#1", err)
	}

	result, err := segments.Parse()
	if err != nil {
		log.Fatal("#1", err)
	}

	// Could be easier to work with a map
	// holding only the values that have been
	// set by the configuration file.
	map1 := result[0].GetParams()
	fmt.Println(map1)
	// Output: map[Host:sub.example.com IdentityFile:~/.ssh/sub.example.com Port:202 User:ssh_config_parser]
}

func TestGetParams(t *testing.T) {
	raw1 := NewRawDataFromString(`# First block
Host sub.example.com
	IdentityFile ~/.ssh/sub.example.com
	Port 202
	User ssh_config_parser
	`)
	input1, _ := raw1.GetSegments()

	raw2 := NewRawDataFromString(`# ChacaS0 - gitlab.com
Host gitlab.com
	HostName gitlab.com
	IdentityFile ~/.ssh/gitlab
	`)
	input2, _ := raw2.GetSegments()

	parsed1, _ := input1.Parse()
	parsed2, _ := input2.Parse()

	testItems := []struct {
		Input         SshConfig
		ExpectedValue map[string]string
	}{
		{
			parsed1[0],
			map[string]string{
				"Host":         "sub.example.com",
				"IdentityFile": "~/.ssh/sub.example.com",
				"Port":         "202",
				"User":         "ssh_config_parser",
			},
		},
		{
			parsed2[0],
			map[string]string{
				"Host":         "gitlab.com",
				"HostName":     "gitlab.com",
				"IdentityFile": "~/.ssh/gitlab",
			},
		},
	}

	for _, v := range testItems {
		if got := v.Input.GetParams(); !cmp.Equal(got, v.ExpectedValue) {
			t.Error("[x] SshConfig.GetParams()\n\tgot:\n\t", got, "\n\n\texpeceted value:\n\t", v.ExpectedValue)
			t.Fail()
		}
	}
}
