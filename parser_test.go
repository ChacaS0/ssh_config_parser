package ssh_config_parser

import (
	"testing"
)

func TestGetConfigIndexByHost(t *testing.T) {

	testItems := []struct {
		Input1        []SshConfig
		Input2        string // host
		ExpectedValue int    // index
		ExpectErr     error
	}{
		{
			// with many entry in input1
			[]SshConfig{
				SshConfig{Host: "test.com"},
				SshConfig{Host: "test_2.com"},
				SshConfig{Host: "not_my_jam.io"},
				SshConfig{Host: "gitlab.com"},
				SshConfig{Host: "github.com"},
			},
			"gitlab.com",
			3,
			nil,
		},
		{
			// with a single entry in input1
			[]SshConfig{
				SshConfig{Host: "gitlab.com"},
			},
			"gitlab.com",
			0,
			nil,
		},
		{
			// host not in input1
			[]SshConfig{
				SshConfig{Host: "test.com"},
				SshConfig{Host: "test_2.com"},
				SshConfig{Host: "not_my_jam.io"},
				SshConfig{Host: "gitlab.com"},
				SshConfig{Host: "github.com"},
			},
			"bitbucket.com",
			-1,
			ErrHostNotFound,
		},
		{
			// Empty input1 test
			[]SshConfig{},
			"gitlab.com",
			-1,
			ErrHostNotFound,
		},
		{
			// Empty input2 test
			[]SshConfig{
				SshConfig{Host: "gitlab.com"},
			},
			"",
			-1,
			ErrHostNotFound,
		},
	}

	for _, v := range testItems {
		if got, err := GetConfigIndexByHost(v.Input1, v.Input2); got != v.ExpectedValue || err != v.ExpectErr {
			t.Error("[x] GetConfigIndexByHost()\n\tgot:\n\t", got, "\n\n\texpeceted value:\n\t", v.ExpectedValue, "\n\n\tgot error:\n\t", err, "\n\texpected error:\n\t", v.ExpectErr)
			t.Fail()
		}
	}
}
