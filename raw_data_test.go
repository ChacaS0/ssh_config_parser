package ssh_config_parser

import (
	"testing"
)

func TestSegmentSections(t *testing.T) {
	testItems := []struct {
		Input         RawData
		ExpectedValue Segments
		ExpectErr     error
	}{
		{
			Input: RawData{[]byte(`# First block
Host sub.example.com
	IdentityFile ~/.ssh/sub.example.com
	Port 202
	User ssh_config_parser

# ChacaS0 - gitlab.com
Host gitlab.com
	HostName gitlab.com
	IdentityFile ~/.ssh/gitlab
			`)},
			ExpectedValue: Segments{[]Segment{
				NewSegmentFromString(`Host sub.example.com
	IdentityFile ~/.ssh/sub.example.com
	Port 202
	User ssh_config_parser`),
				NewSegmentFromString(`Host gitlab.com
	HostName gitlab.com
	IdentityFile ~/.ssh/gitlab`),
			}},
			ExpectErr: nil,
		},
		{
			Input:         RawData{[]byte(``)},
			ExpectedValue: Segments{},
			ExpectErr:     ErrNotingSegmented,
		},
	}

	for _, v := range testItems {
		if got, err := v.Input.GetSegments(); !got.IsEqual(v.ExpectedValue) || err != v.ExpectErr {
			t.Error("[x] SshConfig.SegmentSections()\n\tgot:\n\t", got, "\n\n\texpeceted value:\n\t", v.ExpectedValue, "\n\n\tgot error:\n\t", err, "\n\texpected error:\n\t", v.ExpectErr)
			t.Fail()
		}
	}
}
