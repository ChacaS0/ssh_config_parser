# SSH Config Parser

## Description
This modules is a simple way to parse SSH configuration files (client side). 
It will be able to parse the files like the default `~/.ssh/config`.

## Documentation
Checkout the [![godoc_reference](https://pkg.go.dev/badge/gitlab.com/ChacaS0/ssh_config_parser.svg)](https://pkg.go.dev/gitlab.com/ChacaS0/ssh_config_parser).

## Example

A basic usage example is :

```go
package main

import(
	"fmt"
	"log"

	scp "gitlab.com/ChacaS0/ssh_config_parser"
)

func main() {
	// Make a RawData as the input to use
	raw := scp.NewRawDataFromString(`# First block
Host sub.example.com
	IdentityFile ~/.ssh/sub.example.com
	Port 202
	User ssh_config_parser
	# ChacaS0 - gitlab.com
Host gitlab.com
	HostName gitlab.com
	IdentityFile ~/.ssh/gitlab
	`)
	
	// Break it down into segments
	segments, err := raw.GetSegments()
	if err != scp.ErrNotingSegmented {
		log.Print("No segment found")
	}
	
	// Parse it to get the []SshConfig
	result, err := segments.Parse()
	if err != nil {
		log.Fatal("#2", err)
	}
	
	// Could be easier to work with a map
	// holding only the values that have been
	// set by the configuration file.
	map1 := result[0].GetParams()

	// See the results
	fmt.Println(map1)
}
```